#include "memmanage.h"
#include "hashtable.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
/* Globals, used to make trace work correctly */
int tracecall = 0;              /* are we tracing function entry?  */
int traceexit = 0;              /* are we tracing functino exit?   */
int indent = 1;                 /* how should we indent trace output? */
/* Function Prototypes */
/*  Function:     ReadInput                               */
/*  Input Params: stream -- file to read from             */
/*                next -- one character input buffer      */
/*  Return Value: none                                    */
/*  Purpose:      Read in the next s-expression from the  */
/*                input file stream, and store it on the  */
/*                call stack                              */
void ReadInput(FILE * stream, char *next);
/*  Function:     Exit                                     */
/*  Input Params: sexpr -- s-expression                    */
/*  Return Value: 1 if sexpr is a list whose first element */
/*                  is exit, halt, or quit                 */
int Exit(memptr sexpr);
/*  Function:     CalcResult                               */
/*  Input Params: SymbolTable                              */
/*                sexpr -- s-expression to evalutate       */
/*  Output Params: Error -- true if there is some error    */
/*  Return Value:  none                                    */
/*  Purpose:       This function evaluates sexpr, and      */
/*                 pushes the result onto the call stack   */
void CalcResult(hashTable symbolTable, memptr sexpr, int *error);
/*  Function:     Print                                    */
/*  Input Params: sexpr -- s-expression to print           */
/*  Return Value:  none                                    */
/*  Purpose:       This function prints sexpr to the screen*/
void Print(memptr sexpr);
/*  Function:     PrintList                                */
/*  Input Params: sexpr -- s-expression to print           */
/*                first -- true iff printing first element */
/*                         in the list (to make spaces     */
/*                         pretty                          */
/*  Return Value:  none                                    */
/*  Purpose:       This function prints sexpr to the screen*/
/*                 when sexpr is a list                    */
void PrintList(int first, memptr sexpr);
/*  Function:     SkipSpaces                                */
/*  Input Params: stream -- file pointer                    */
/*                next  -- one char lookahead buffer        */
/*  Return Value:  none                                    */
/*  Purpose:       Skip over spaces in the input file      */
void SkipSpaces(FILE * stream, char *next);
/*  Function:     AtomChar                                  */
/*  Input Params: letter -- char to test                    */
/*  Purpose:      returns true iff letter is a valid char   */
/*                for an atom                               */
int AtomChar(char letter);
/*  Function:     AtomChar                                 */
/*  Input Params: e1, e2 s-expressions                     */
/*  Output Params: Error -- true if there is some error    */
/*  Purpose:      returns true iff e1 = e2                 */
void CalcEqual(memptr e1, memptr e2, int *error);
/*  Function:     True                                     */
/*  Input Params: none                                     */
/*  Purpose:      returns s-expression 't                  */
memptr True(void);
/*  Function:     DefineFuntion                            */
/*  Input Params:  symbolTable -- Symbol Table             */
/*                 sexpr -- s-expression of a defun        */
/*  Purpose:       adds function body (with values of      */
/*                 parameters converted to appropriate     */
/*                 offsets) to function table              */
void DefineFunction(hashTable symbolTable, memptr sexpr);
/*  Function:      AddFunctionParamOffets                   */
/*  Input Params:  symbolTable -- Symbol Table              */
/*                 sexpr -- s-expression of a defun         */
/*  Purpose:       adds appropriate offsets to paranmets of */
/*                 the function, in the function body       */
void AddFunctionParamOffsets(hashTable env, memptr body);
void AddFunctionParamOffsetsList(hashTable env, memptr body);
/*  Function:     DoArithmetic                               */
/*  Input Params: sexpr -- a s-expression representing an    */
/*                  arithemetic function call, ie.  (+ 3 4), */
/*                  etc.                                     */
/*  Output Params: Error -- true if there is some error      */
/*  Purpose:       pushes result of operation on the stack   */
void DoArithmetic(hashTable symbolTable, memptr sexpr, int *error);
void DoArithmeticShlomi(hashTable symbolTable, memptr sexpr, int *error);
/*  Function:     PrintCall                                  */
/*  Input Params: sexpr --  function being called            */
/*  Purpose:      for tracing -- print out the call of the funtion  */
void PrintCall(memptr sexpr);
/*  Function:     PrintExit                                  */
/*  Input Params: sexpr --  function being called            */
/*  Purpose:      for tracing -- print out the exit of the funtion  */
void PrintExit(memptr sexpr);
/*  Function:     PrintTr                                                */
/*  Input Params: sexpr -- s-expression to print out                     */
/*  Purpose:      for tracing -- print out an s-expression to the screen */
/*                  (using the trace format)                             */
void PrintTr(memptr sexpr);
void PrintListTr(int first, memptr sexpr);
/*  Function:     CountParams                                */
/*  Input Params: sexpr -- a s-expression representing a     */
/*                  function definition                      */
/*  Output Params: Error -- true if there is some error      */
/*  Return Value:  The number of parameters in the function  */
int CountParams(memptr sexpr, int *error);
/*  Function:     DoLoad                                                  */
/*  Input Params: sexpr -- a s-expression representing                    */
/*                  the filename to load (ie, sexp = (load "foo.lsp"))    */
/*  Output Params: Error -- true if there is some error (can't open file) */
/*  Purpose:       Read in an input file, storing all function defintions */
/*                 on the function stack                                  */
void DoLoad(hashTable symbolTable, memptr sexpr, int *error);
/*  Function:     Error                                                   */
/*  Input Params: message -- a message to print to screen                 */
/*                sexpr -- s-expression representing offending bit of     */
/*                   code (so it can also be printed                      */
/*  Output Params: Error -- set to true                                   */
/*  Purpose:       Print a useful error message to the screen             */
void Error(char *message, memptr sexpr, int *error);
/* Function Definitions  */
int main()
{
    hashTable symbolTable = HashTable(5);
    memptr input, result;
    char first = ' ';
    int error = 0;
    int loop = 1;
    InitMemorySystem();
    printf("Welcome to Simple Lisp :-) \n");
    while (loop) {
        printf(">");
        first = ' ';
        ReadInput(stdin, &first);
        input = callStack[SP - 1];
        if (Exit(input)) {
            loop = 0;
        } else {
            CalcResult(symbolTable, input, &error);
            result = callStack[--SP];
            SP--;
            if (!error)
                Print(result);
            else
                error = 0;
            printf("\n");
        }
    }
    return 0;
}

void ReadInput(FILE * stream, char *next)
{
    char buffer[MAXSTRINGSIZE];
    int index = 0;
    memptr retval;
    memptr temp;
    SkipSpaces(stream, next);
    if (*next == '(') {
        *next = getc(stream);
        SkipSpaces(stream, next);
        if (*next == ')') {
            *next = getc(stream);
            callStack[SP++] = 0;
            return;
        }
        callStack[SP++] = New();
        MemoryPool[callStack[SP - 1]].carKind = MemoryPool[callStack[SP - 1]].cdrKind = CONS;
        ReadInput(stream, next);
        MemoryPool[callStack[SP - 2]].car = callStack[SP - 1];
        SP--;
        temp = callStack[SP - 1];
        SkipSpaces(stream, next);
        while (*next != ')') {
            MemoryPool[temp].cdr = New();
            temp = MemoryPool[temp].cdr;
            MemoryPool[temp].carKind = MemoryPool[temp].cdrKind = CONS;
            ReadInput(stream, next);
            MemoryPool[temp].car = callStack[--SP];
            SkipSpaces(stream, next);
            MemoryPool[temp].cdr = 0;
        }
        *next = getc(stream);
        MemoryPool[temp].cdr = 0;
    } else if (*next == '\'') {
        *next = getc(stream);
        callStack[SP++] = New();
        MemoryPool[callStack[SP - 1]].carKind = MemoryPool[callStack[SP - 1]].cdrKind = CONS;
        temp = New();
        MemoryPool[temp].carKind = STRING;
        MemoryPool[temp].car = temp;
        MemoryPool[temp].cdrKind = NIL;
        MemoryPool[temp].cdr = 0;
        AtomHandles[temp].value = -1;
        AtomHandles[temp].name = malloc(sizeof(char) * 6);
        strcpy(AtomHandles[temp].name, "quote");
        MemoryPool[callStack[SP - 1]].car = temp;
        MemoryPool[callStack[SP - 1]].cdr = New();
        temp = MemoryPool[callStack[SP - 1]].cdr;
        MemoryPool[temp].carKind = MemoryPool[temp].cdrKind = CONS;
        ReadInput(stream, next);
        MemoryPool[temp].car = callStack[--SP];
        MemoryPool[temp].cdr = 0;
    } else if (*next == '\"') {
        buffer[index++] = '\"';
        do
            buffer[index++] = getc(stream);
        while (buffer[index - 1] != '\"' && index < MAXSTRINGSIZE);
        *next = ' ';
        if (index == MAXSTRINGSIZE)
            printf("Strings have a maximum length of %d characters", MAXSTRINGSIZE);
        buffer[index] = '\0';
        temp = New();
        MemoryPool[temp].carKind = STRING;
        MemoryPool[temp].car = temp;
        MemoryPool[temp].cdrKind = NIL;
        MemoryPool[temp].cdr = 0;
        AtomHandles[temp].value = -1;
        AtomHandles[temp].name = malloc(sizeof(char) * 6);
        strcpy(AtomHandles[temp].name, buffer);
        callStack[SP++] = temp;
    } else {
        buffer[index++] = *next;
        do
            buffer[index++] = getc(stream);
        while (AtomChar(buffer[index - 1]));
        index--;
        *next = buffer[index];
        buffer[index] = '\0';
        temp = New();
        if (isdigit(buffer[0])
            || ((buffer[0] == '-') && (isdigit(buffer[1])))) {
            MemoryPool[temp].carKind = INTEGER;
            MemoryPool[temp].car = atoi(buffer);
            MemoryPool[temp].cdrKind = NIL;
            MemoryPool[temp].cdr = 0;
        } else {
            MemoryPool[temp].carKind = STRING;
            MemoryPool[temp].car = temp;
            MemoryPool[temp].cdrKind = NIL;
            MemoryPool[temp].cdr = 0;
            AtomHandles[temp].value = -1;
            AtomHandles[temp].name = malloc(sizeof(char) * (index + 1));
            strcpy(AtomHandles[temp].name, buffer);
        }
        callStack[SP++] = temp;
    }
    return;
}

void SkipSpaces(FILE * stream, char *next)
{
    while (isspace(*next)) {
        *next = getc(stream);
    }
    if (*next == ';') {
        *next = getc(stream);
        while (*next != '\n')
            *next = getc(stream);
        SkipSpaces(stream, next);
    }
}

void PrintList(int first, memptr sexpr)
{
    if (sexpr != 0) {
#if 0
        if (Memory[sexpr].kind != List) {
            printf(" . ");
            Print(sexpr);
        } else {
            if (!first)
                printf(" ");
            Print(Memory[sexpr].u.list.car);
            PrintList(0, Memory[sexpr].u.list.cdr);
        }
#endif
        if (MemoryPool[sexpr].carKind != CONS && MemoryPool[sexpr].cdrKind != CONS) {
            /* It is not a list: */
            printf(" . ");
            Print(sexpr);
        } else {
            if (!first)
                printf(" ");
            Print(MemoryPool[sexpr].car);
            PrintList(0, MemoryPool[sexpr].cdr);
        }
    }
}

void Print(memptr sexpr)
{
    if (sexpr != 0) {
        if (MemoryPool[sexpr].carKind == STRING) {
            /* It is an Atom = string */
            if (AtomHandles[sexpr].value >= 0)
                printf("_p%d", AtomHandles[sexpr].value);
            else
                printf("%s", AtomHandles[sexpr].name);
        } else if (MemoryPool[sexpr].carKind == INTEGER) {
            printf("%d", MemoryPool[sexpr].car);
        } else if (MemoryPool[sexpr].carKind == CONS && MemoryPool[sexpr].cdrKind == CONS) {
            /* It is a list: */
            printf("(");
            PrintList(1, sexpr);
            printf(")");
        }
        fflush(stdin);
    } else
        printf("()");
}

int Exit(memptr sexpr)
{
    if (sexpr == 0)
        return 0;
    if (MemoryPool[sexpr].carKind == CONS && MemoryPool[sexpr].cdrKind == CONS)
        /* It is a list */
        if (MemoryPool[MemoryPool[sexpr].car].carKind == STRING)
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "quit") == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "exit")
                    == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "halt")
                    == 0))
                return 1;
    return 0;
}

memptr True(void)
{
    memptr returnVal = New();
    MemoryPool[returnVal].carKind = STRING;
    MemoryPool[returnVal].car = returnVal;
    AtomHandles[returnVal].value = -1;
    AtomHandles[returnVal].name = malloc(sizeof(char) * 2);
    strcpy(AtomHandles[returnVal].name, "t");
    return returnVal;
}

void CalcEqual(memptr e1, memptr e2, int *error)
{
    memptr tmpResult;
    if (*error) {
        callStack[SP++] = 0;
        return;
    }
    if ((e1 == 0) && (e2 == 0)) {
        callStack[SP++] = True();
        return;
    }
    if ((e1 == 0) || (e2 == 0)) {
        callStack[SP++] = 0;
        return;
    }
    if ((MemoryPool[e1].carKind != MemoryPool[e2].carKind) && (MemoryPool[e1].cdrKind != MemoryPool[e2].cdrKind)) {
        callStack[SP++] = 0;
        return;
    }
    if (MemoryPool[e1].carKind == STRING) {
        callStack[SP++] = strcmp(AtomHandles[e1].name, AtomHandles[e2].name) != 0 ? 0 : True();
        return;
    } else if (MemoryPool[e1].carKind == INTEGER) {
        if (MemoryPool[e1].car != MemoryPool[e2].car)
            callStack[SP++] = 0;
        else
            callStack[SP++] = True();
        return;
    } else if (MemoryPool[e1].carKind == CONS && MemoryPool[e1].cdrKind == CONS) {
        /* It is a list: */
        CalcEqual(MemoryPool[e1].car, MemoryPool[e2].car, error);
        if (callStack[SP - 1] == 0) {
            return;
            SP--;
            CalcEqual(MemoryPool[e1].cdr, MemoryPool[e2].cdr, error);
        } else
            callStack[SP++] = 0;
        return;
    }
}

void DoLoad(hashTable symbolTable, memptr sexpr, int *error)
{
    char buffer = ' ';
    char fileName[100];
    FILE *infile;
    int index;
    memptr input;
    char *tfn = AtomHandles[MemoryPool[MemoryPool[sexpr].cdr].car].name;
    if (tracecall)
        PrintCall(sexpr);
    indent++;
    if (tfn[0] != '\"')
        strcpy(fileName, tfn);
    else {
        for (index = 0; tfn[index + 1] != '\"'; index++)
            fileName[index] = tfn[index + 1];
        fileName[index] = '\0';
    }
    infile = fopen(fileName, "r");
    if (infile == NULL) {
        printf("Could not open file %s\n", fileName);
        *error = 1;
        callStack[SP++] = 0;
        indent--;
        return;
    }
    printf("Reading file %s\n", fileName);
    fflush(stdin);
    SkipSpaces(infile, &buffer);
    while (!feof(infile)) {
        SkipSpaces(infile, &buffer);
        ReadInput(infile, &buffer);
        input = callStack[--SP];
        CalcResult(symbolTable, input, error);
        SP--;
        if (!feof(infile))
            SkipSpaces(infile, &buffer);
    }
    fclose(infile);
    callStack[SP++] = 0;
    if (traceexit)
        PrintExit(sexpr);
    indent--;
    return;
}

void CalcResult(hashTable symbolTable, memptr sexpr, int *error)
{
    memptr t1, t2, t3;
    int numParam = 0;
    /* --- If there has already been an error, place 0 on the stack -- */
    /* ---    and return (stop computation)                         -- */
    if (*error) {
        callStack[SP++] = 0;
        return;
    }
    /* --- Number -- return the correct value --- */
    if ((sexpr == 0) || MemoryPool[sexpr].carKind == INTEGER) {
        callStack[SP++] = sexpr;
        return;
    }
    /* --- String, evaluates to itself  --- */
    if (MemoryPool[sexpr].carKind == STRING && AtomHandles[sexpr].name[0] == '\"') {
        callStack[SP++] = sexpr;
        return;
    }
    /* --- t (true), evaluates to itself  --- */
    if (MemoryPool[sexpr].carKind == STRING && strcmp(AtomHandles[sexpr].name, "t") == 0) {
        callStack[SP++] = sexpr;
        return;
    }
    if (MemoryPool[sexpr].carKind == STRING && strcmp(AtomHandles[sexpr].name, "nil") == 0) {
        callStack[SP++] = 0;
        return;
    }
    /*  ------- Functions  ---------- */
    if (MemoryPool[sexpr].carKind == CONS && MemoryPool[sexpr].cdrKind == CONS) {
        /* It is a list: */
        if ((MemoryPool[sexpr].car != 0) && (MemoryPool[MemoryPool[sexpr].car].carKind == STRING)) {
            numParam = CountParams(sexpr, error);
            /* ------ Built in functions -------- */
            /* ------ defun -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "defun") == 0) && (numParam == 3)) {
                DefineFunction(symbolTable, sexpr);
                callStack[SP++] = 0;
                return;
            }
            /* ------ trace -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "trace") == 0) && numParam == 0) {
                printf("Tracing on \n");
                tracecall = 1;
                traceexit = 1;
                indent = 1;
                callStack[SP++] = 0;
                return;
            }
            /* ------ gc -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "gc") == 0) && (numParam == 0)) {
                CollectGarbage();
                callStack[SP++] = 0;
                return;
            }
            /* ----- printmem ------ */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "printmem") == 0) && (numParam < 2)) {
                if (MemoryPool[sexpr].cdr == 0)
                    PrintMemory(MEMSIZE - 1);
                else {
                    if (MemoryPool[sexpr].cdr == 0 || MemoryPool[MemoryPool[sexpr].cdr].car == 0 || MemoryPool[MemoryPool[MemoryPool[sexpr].cdr].car].carKind != INTEGER) {
                        printf("Error: printmem takes a single integer as an input parameter");
                        *error = 1;
                    } else
                        PrintMemory(MemoryPool[MemoryPool[MemoryPool[sexpr].cdr].car].car);
                }
                callStack[SP++] = 0;
                return;
            }
            /* --------- print ------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "print") == 0) && (numParam < 2)) {
                if (AtomHandles[MemoryPool[MemoryPool[sexpr].cdr].car].name != NULL)
                    //printf("%s", AtomHandles[MemoryPool[MemoryPool[sexpr].cdr].car].name);
                    callStack[SP++] = MemoryPool[MemoryPool[sexpr].cdr].car;
                else
                    callStack[SP++] = 0;
                return;
            }
            /* ------ notrace -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "notrace") == 0) && (numParam == 0)) {
                printf("Tracing off \n");
                tracecall = 0;
                traceexit = 0;
                callStack[SP++] = 0;
                return;
            }
            /* ------ load -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "load") == 0) && (numParam == 1)) {
                DoLoad(symbolTable, sexpr, error);
                return;
            }
            /* ------ quote -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "quote") == 0) && (numParam == 1)) {
                callStack[SP++] = MemoryPool[MemoryPool[sexpr].cdr].car;
                return;
            }
            /* ------- Predicate functions ---------- */
            /* ------- null and not ------------------ */
            if (((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "null")
                  == 0)
                 || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "not")
                     == 0)) && (numParam == 1)) {
                if (tracecall)
                    PrintCall(sexpr);
                indent++;
                CalcResult(symbolTable, MemoryPool[MemoryPool[sexpr].cdr].car, error);
                t1 = callStack[--SP];
                if (t1 == 0)
                    callStack[SP++] = True();
                else
                    callStack[SP++] = 0;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* ------- number ---------------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "number") == 0) && (numParam == 1)) {
                if (tracecall)
                    PrintCall(sexpr);
                indent++;
                CalcResult(symbolTable, MemoryPool[MemoryPool[sexpr].cdr].car, error);
                t1 = callStack[--SP];
                if (t1 == 0)
                    callStack[SP++] = 0;
                else if (MemoryPool[t1].carKind == INTEGER)
                    callStack[SP++] = True();
                else
                    callStack[SP++] = 0;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* -------- atom ----------------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "atom") == 0) && (numParam == 1)) {
                if (tracecall) {
                    PrintCall(sexpr);
                }
                indent++;
                CalcResult(symbolTable, MemoryPool[MemoryPool[sexpr].cdr].car, error);
                t1 = callStack[--SP];
                if (t1 == 0)
                    callStack[SP++] = True();
                else if (MemoryPool[t1].carKind != CONS && MemoryPool[t1].cdrKind != CONS)
                    callStack[SP++] = True();
                else
                    callStack[SP++] = 0;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* -------- list ----------------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "list") == 0) && (numParam == 1)) {
                if (tracecall) {
                    PrintCall(sexpr);
                }
                indent++;
                CalcResult(symbolTable, MemoryPool[MemoryPool[sexpr].cdr].car, error);
                t1 = callStack[--SP];
                if (t1 == 0)
                    callStack[SP++] = True();
                else if (MemoryPool[t1].carKind == CONS && MemoryPool[t1].cdrKind == CONS)
                    callStack[SP++] = True();
                else
                    callStack[SP++] = 0;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* ------ equal -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "equal") == 0) && (numParam == 2)) {
                if (tracecall) {
                    PrintCall(sexpr);
                }
                indent++;
                t1 = MemoryPool[sexpr].cdr;
                t2 = MemoryPool[t1].car;
                t1 = MemoryPool[t1].cdr;
                t1 = MemoryPool[t1].car;
                CalcResult(symbolTable, t2, error);
                CalcResult(symbolTable, t1, error);
                t1 = callStack[SP - 1];
                t2 = callStack[SP - 2];
                SP = SP - 2;
                CalcEqual(t1, t2, error);
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* ------ and -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "and") == 0) && (numParam == 2)) {
                if (tracecall)
                    PrintCall(sexpr);
                indent++;
                t1 = MemoryPool[sexpr].cdr;
                t2 = MemoryPool[t1].car;
                t1 = MemoryPool[t1].cdr;
                t1 = MemoryPool[t1].car;
                CalcResult(symbolTable, t2, error);
                CalcResult(symbolTable, t1, error);
                t1 = callStack[SP - 1];
                t2 = callStack[SP - 2];
                SP = SP - 2;
                if ((t1 != 0) && (t2 != 0)) {
                    callStack[SP++] = True();
                    return;
                } else {
                    callStack[SP++] = 0;
                }
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* ------ or -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "or")
                 == 0) && (numParam == 2)) {
                if (tracecall) {
                    PrintCall(sexpr);
                }
                indent++;
                t1 = MemoryPool[sexpr].cdr;
                t2 = MemoryPool[t1].car;
                t1 = MemoryPool[t1].cdr;
                t1 = MemoryPool[t1].car;
                CalcResult(symbolTable, t2, error);
                CalcResult(symbolTable, t1, error);
                t1 = callStack[SP - 1];
                t2 = callStack[SP - 2];
                SP = SP - 2;
                if ((t1 != 0) || (t2 != 0)) {
                    callStack[SP++] = True();
                    return;
                } else {
                    callStack[SP++] = 0;
                }
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* ------ List Functions: cons -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "cons") == 0) && (numParam == 2)) {
                if (tracecall) {
                    PrintCall(sexpr);
                }
                indent++;
                t1 = MemoryPool[sexpr].cdr;
                t2 = MemoryPool[t1].cdr;
                CalcResult(symbolTable, MemoryPool[t1].car, error);
                t1 = callStack[SP - 1];
                CalcResult(symbolTable, MemoryPool[t2].car, error);
                t2 = callStack[SP - 1];
                t3 = New();
                MemoryPool[t3].carKind = CONS;
                MemoryPool[t3].cdrKind = CONS;
                MemoryPool[t3].car = t1;
                MemoryPool[t3].cdr = t2;
                SP = SP - 2;
                callStack[SP++] = t3;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* ------ List Functions: car -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "car") == 0) && (numParam == 1)) {
                if (tracecall)
                    PrintCall(sexpr);
                indent++;
                t1 = MemoryPool[sexpr].cdr;
                CalcResult(symbolTable, MemoryPool[t1].car, error);
                t2 = callStack[--SP];
                if (t2 == 0 || (MemoryPool[t2].carKind != CONS && MemoryPool[t2].cdrKind != CONS)) {
                    printf("Error: car requires a non-null list: (car ");
                    PrintTr(t2);
                    printf(")\n");
                    *error = 1;
                    callStack[SP++] = 0;
                    if (traceexit)
                        PrintExit(sexpr);
                    indent--;
                    return;
                }
                callStack[SP++] = MemoryPool[t2].car;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            /* ------ List Functions: cdr -------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "cdr") == 0) && (numParam == 1)) {
                if (tracecall)
                    PrintCall(sexpr);
                indent++;
                t1 = MemoryPool[sexpr].cdr;
                CalcResult(symbolTable, MemoryPool[t1].car, error);
                t2 = callStack[--SP];
                if (t2 == 0 || (MemoryPool[t2].carKind != CONS && MemoryPool[t2].cdrKind != CONS)) {
                    printf("Error: cdr requires a non-null list : (cdr ");
                    PrintTr(t2);
                    printf(")\n");
                    callStack[SP++] = 0;
                    *error = 1;
                    if (traceexit)
                        PrintExit(sexpr);
                    indent--;
                    return;
                }
                callStack[SP++] = MemoryPool[t2].cdr;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
      /*************    Branching : If    *******************/
            if (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "if")
                == 0) {
                t1 = MemoryPool[sexpr].cdr;
                if (MemoryPool[t1].carKind != CONS && MemoryPool[t1].cdrKind != CONS) {
                    Error("Malformed if :", sexpr, error);
                    return;
                }
                t2 = MemoryPool[t1].car;
                if ((MemoryPool[t2].carKind != CONS && MemoryPool[t2].cdrKind != CONS) && (strcmp(AtomHandles[t2].name, "nil") != 0)) {
                    Error("Malformed if :", sexpr, error);
                    return;
                }
                CalcResult(symbolTable, MemoryPool[t1].car, error);
                t3 = callStack[--SP];
                if ((t3 != 0) && (MemoryPool[t3].carKind == STRING) && (AtomHandles[t3].name[0] == 't')) {
                    CalcResult(symbolTable, MemoryPool[MemoryPool[t1].cdr].car, error);
                    return;
                } else {
                    CalcResult(symbolTable, MemoryPool[MemoryPool[MemoryPool[t1].cdr].cdr].car, error);
                    return;
                }
                // When does this execute?
                callStack[SP++] = 0;
                return;
            }
            /* ----- Branching : cond ---------- */
            if (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "cond")
                == 0) {
                t1 = MemoryPool[sexpr].cdr;
                while (t1) {
                    if (MemoryPool[t1].carKind != CONS && MemoryPool[t1].cdrKind != CONS) {
                        Error("Malformed cond :", sexpr, error);
                        return;
                    }
                    t2 = MemoryPool[t1].car;
                    if ((MemoryPool[t2].carKind != CONS && MemoryPool[t2].cdrKind != CONS)
                        || (MemoryPool[t2].cdr == 0)
                        || (MemoryPool[MemoryPool[t2].cdr].carKind != CONS && MemoryPool[MemoryPool[t2].cdr].cdrKind != CONS)
                        || (MemoryPool[MemoryPool[t2].cdr].cdr != 0)) {
                        Error("Malformed cond :", sexpr, error);
                        return;
                    }
                    CalcResult(symbolTable, MemoryPool[t2].car, error);
                    t3 = callStack[--SP];
                    if (t3 != 0) {
                        CalcResult(symbolTable, MemoryPool[MemoryPool[t2].cdr].car, error);
                        return;
                    } else {
                        t1 = MemoryPool[t1].cdr;
                    }
                }
                callStack[SP++] = 0;
                return;
            }
            /* ------- Arithemetic functions ---------- */
            if ((strcmp(AtomHandles[MemoryPool[sexpr].car].name, "+")
                 == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "-")
                    == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "*")
                    == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "/")
                    == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, ">")
                    == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "<")
                    == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "%")
                    == 0)
                || (strcmp(AtomHandles[MemoryPool[sexpr].car].name, "=")
                    == 0)) {
                if (numParam <= 2)
                    DoArithmetic(symbolTable, sexpr, error);
                else
                    DoArithmeticShlomi(symbolTable, sexpr, error);
                return;
            }
            /* ------- User Defined functions ---------- */
            {
                char functionName[MAXSTRINGSIZE + 1];
                int i;
                strcpy(functionName, AtomHandles[MemoryPool[sexpr].car].name);
                for (i = 0; functionName[i] != '\0'; i++);
                functionName[i] = numParam;
                functionName[i + 1] = '\0';
                if (Exists(symbolTable, functionName)) {
                    int currentParam = 1;
                    int oldFP;
                    int oldSP;
                    t1 = functionStack[Find(symbolTable, functionName)];
                    if (tracecall)
                        PrintCall(sexpr);
                    indent++;
                    oldSP = SP;
                    SP = SP + numParam;
                    for (t2 = MemoryPool[sexpr].cdr; t2; t2 = MemoryPool[t2].cdr) {
                        CalcResult(symbolTable, MemoryPool[t2].car, error);
                        callStack[oldSP + numParam - currentParam] = callStack[--SP];
                        currentParam++;
                    }
                    oldFP = FP;
                    FP = oldSP + numParam;;
                    CalcResult(symbolTable, t1, error);
                    FP = oldFP;
                    callStack[SP - numParam - 1] = callStack[SP - 1];
                    SP = SP - numParam;
                    if (traceexit)
                        PrintExit(sexpr);
                    indent--;
                    return;
                } else {
                    printf("Unknown function : %s/%d\n", AtomHandles[MemoryPool[sexpr].car].name, numParam);
                    callStack[SP++] = 0;
                    *error = 1;
                    return;
                }
            }                   /* ------- User Defined functions ---------- */
        }
    }                           /* Functions */
    if (MemoryPool[sexpr].carKind == STRING) {
        if (AtomHandles[sexpr].value < 0) {
            if (Exists(symbolTable, AtomHandles[sexpr].name)) {
                callStack[SP++] = functionStack[Find(symbolTable, AtomHandles[sexpr].name)];
                return;
            } else {
                callStack[SP++] = 0;
                printf("Error -- symbol %s has no value \n", AtomHandles[sexpr].name);
                *error = 1;
                return;
            }
        } else {
            callStack[SP++] = callStack[FP - AtomHandles[sexpr].value];
            return;
        }
    }
    printf("Invalid Expression : ");
    PrintTr(sexpr);
    printf("\n");
    *error = 1;
    callStack[SP++] = 0;
    return;
}

void DoArithmetic(hashTable symbolTable, memptr sexpr, int *error)
{
    memptr t1, t2, t3;
    if (*error) {
        callStack[SP++] = 0;
        return;
    }
    if (tracecall) {
        PrintCall(sexpr);
    }
    indent++;
    t1 = MemoryPool[sexpr].cdr;
    t2 = MemoryPool[t1].cdr;
    CalcResult(symbolTable, MemoryPool[t1].car, error);
    if (*error)
        return;
    t1 = callStack[--SP];
    CalcResult(symbolTable, MemoryPool[t2].car, error);
    if (*error)
        return;
    t2 = callStack[--SP];
    if (t1 == 0 || t2 == 0 || MemoryPool[t1].carKind != INTEGER || MemoryPool[t2].carKind != INTEGER) {
        printf("Error: Numeric operations require numbers. ");
        PrintTr(sexpr);
        printf("\n");
        *error = 1;
        callStack[SP++] = 0;
        if (traceexit)
            PrintExit(sexpr);
        indent--;
        return;
    }
    t3 = New();
    MemoryPool[t3].carKind = INTEGER;
    MemoryPool[t3].cdrKind = NIL;
    MemoryPool[t3].cdr = 0;
    switch (AtomHandles[MemoryPool[sexpr].car].name[0]) {
    case '+':
        MemoryPool[t3].car = MemoryPool[t1].car + MemoryPool[t2].car;
        break;
    case '-':
        MemoryPool[t3].car = MemoryPool[t1].car - MemoryPool[t2].car;
        break;
    case '*':
        MemoryPool[t3].car = MemoryPool[t1].car * MemoryPool[t2].car;
        break;
    case '/':
        MemoryPool[t3].car = MemoryPool[t1].car / MemoryPool[t2].car;
        break;
    case '%':
        MemoryPool[t3].car = MemoryPool[t1].car % MemoryPool[t2].car;
        break;
    case '>':
        if (MemoryPool[t1].car > MemoryPool[t2].car) {
            callStack[SP++] = True();
            if (traceexit)
                PrintExit(sexpr);
            indent--;
            return;
        }
        callStack[SP++] = 0;
        if (traceexit)
            PrintExit(sexpr);
        indent--;
        return;
    case '<':
        if (MemoryPool[t1].car < MemoryPool[t2].car) {
            callStack[SP++] = True();
            return;
        }
        callStack[SP++] = 0;
        if (traceexit)
            PrintExit(sexpr);
        indent--;
        return;
    case '=':
        if (MemoryPool[t1].car == MemoryPool[t2].car) {
            callStack[SP++] = True();
            return;
        }
        callStack[SP++] = 0;
        if (traceexit)
            PrintExit(sexpr);
        indent--;
        return;
    case 't':
        callStack[SP++] = True();
        return;
    }
    callStack[SP++] = t3;
    if (traceexit)
        PrintExit(sexpr);
    indent--;
    return;
}

void DoArithmeticShlomi(hashTable symbolTable, memptr sexpr, int *error)
{
    memptr t, tmp, acc, t2;
    int localAcc = 0;
    if (*error) {
        callStack[SP++] = 0;
        return;
    }
    if (tracecall)
        PrintCall(sexpr);
    indent++;
    tmp = MemoryPool[sexpr].cdr;
    if ((AtomHandles[MemoryPool[sexpr].car].name[0] == '=') || (AtomHandles[MemoryPool[sexpr].car].name[0] == '*'))
        localAcc = 1;
    if (AtomHandles[MemoryPool[sexpr].car].name[0] == '/')
        localAcc = MemoryPool[MemoryPool[tmp].car].car * MemoryPool[MemoryPool[tmp].car].car;
    while (tmp != 0) {
        if (MemoryPool[tmp].carKind != CONS && MemoryPool[tmp].cdrKind != CONS) {
            *error = 1;
            printf("Malformed parameter list : ");
            Print(sexpr);
            printf("\n");
        } else {
            CalcResult(symbolTable, MemoryPool[tmp].car, error);
            if (*error)
                return;
            t = callStack[--SP];
            if (t == 0 || MemoryPool[t].carKind != INTEGER) {
                printf("Error: Numeric operations require numbers. ");
                PrintTr(sexpr);
                printf("\n");
                *error = 1;
                callStack[SP++] = 0;
                if (traceexit)
                    PrintExit(sexpr);
                indent--;
                return;
            }
            switch (AtomHandles[MemoryPool[sexpr].car].name[0]) {
            case '+':
                localAcc += MemoryPool[t].car;
                break;
            case '-':
                localAcc -= MemoryPool[t].car;
                break;
            case '*':
                localAcc *= MemoryPool[t].car;
                break;
            case '/':
                localAcc /= MemoryPool[t].car;
                break;
            case '%':
                localAcc %= MemoryPool[t].car;
                break;
            case '>':
                t2 = MemoryPool[tmp].cdr;
                if (t2 != 0) {
                    CalcResult(symbolTable, MemoryPool[t2].car, error);
                    if (*error)
                        return;
                    t2 = callStack[--SP];
                    if (MemoryPool[t].car > MemoryPool[t2].car) {
                        localAcc = 1;
                    } else {
                        callStack[SP++] = 0;
                        if (traceexit)
                            PrintExit(sexpr);
                        indent--;
                        return;
                    }
                }
                break;
            case '<':
                t2 = MemoryPool[tmp].cdr;
                if (t2 != 0) {
                    CalcResult(symbolTable, MemoryPool[t2].car, error);
                    if (*error)
                        return;
                    t2 = callStack[--SP];
                    if (MemoryPool[t].car < MemoryPool[t2].car) {
                        localAcc = 1;
                    } else {
                        callStack[SP++] = 0;
                        if (traceexit)
                            PrintExit(sexpr);
                        indent--;
                        return;
                    }
                }
                break;
            case 't':
                callStack[SP++] = True();
                return;
            }
            tmp = MemoryPool[tmp].cdr;
        }                       /* Else */
    }                           /* While */
    acc = New();
    MemoryPool[acc].carKind = INTEGER;
    MemoryPool[acc].cdrKind = NIL;
    MemoryPool[acc].cdr = 0;
    MemoryPool[acc].car = localAcc;
    if (localAcc == 1 && ((AtomHandles[MemoryPool[sexpr].car].name[0] == '=') || (AtomHandles[MemoryPool[sexpr].car].name[0] == '>') || (AtomHandles[MemoryPool[sexpr].car].name[0] == '<')))
        callStack[SP++] = True();
    else
        callStack[SP++] = acc;
    if (traceexit)
        PrintExit(sexpr);
    indent--;
    return;
}

void AddFunctionParamOffsets(hashTable env, memptr body)
{
    if (body != 0 && MemoryPool[body].carKind != INTEGER) {
        if (MemoryPool[body].carKind == STRING) {
            if (Exists(env, AtomHandles[body].name)) {
                AtomHandles[body].value = Find(env, AtomHandles[body].name);
            } else {
                /* Atom has no value */
            }
        } else {
            /* Memory[body].kind == List */
            if (MemoryPool[body].car != 0 && MemoryPool[MemoryPool[body].car].carKind == STRING) {
                if (strcmp(AtomHandles[MemoryPool[body].car].name, "quote")
                    != 0)
                    AddFunctionParamOffsetsList(env, MemoryPool[body].cdr);
            } else {
                AddFunctionParamOffsets(env, MemoryPool[body].car);
                AddFunctionParamOffsetsList(env, MemoryPool[body].cdr);
            }
        }
    }
}

void AddFunctionParamOffsetsList(hashTable env, memptr body)
{
    if (body != 0) {
        AddFunctionParamOffsets(env, MemoryPool[body].car);
        AddFunctionParamOffsetsList(env, MemoryPool[body].cdr);
    }
}

void DefineFunction(hashTable symbolTable, memptr sexpr)
{
    memptr name;
    memptr params;
    memptr body;
    char functionName[30];
    memptr tmp, tmp2;
    int numParams = 0;
    int i;
    int funcStackIndex;
    hashTable env = HashTable(7);
    name = MemoryPool[MemoryPool[sexpr].cdr].car;
    params = MemoryPool[MemoryPool[MemoryPool[sexpr].cdr].cdr].car;
    body = MemoryPool[MemoryPool[MemoryPool[MemoryPool[sexpr].cdr].cdr].cdr].car;
    for (tmp = params; tmp; tmp = MemoryPool[tmp].cdr)
        Add(env, AtomHandles[MemoryPool[tmp].car].name, ++numParams);
    AddFunctionParamOffsets(env, body);
    strcpy(functionName, AtomHandles[name].name);
    for (i = 0; functionName[i] != '\0'; i++);
    functionName[i] = numParams;
    functionName[i + 1] = '\0';
    if (Exists(symbolTable, functionName)) {
        funcStackIndex = Find(symbolTable, functionName);
        functionStack[funcStackIndex] = body;
        printf("Redefining function %s", AtomHandles[name].name);
        printf("\n");
    } else {
        funcStackIndex = functionStackPtr++;
        Add(symbolTable, functionName, funcStackIndex);
        functionStack[funcStackIndex] = body;
        printf("Defining function %s", AtomHandles[name].name);
        printf("\n");
    }
    Destroy(env);
}

int AtomChar(char letter)
{
    return isalpha(letter) || isdigit(letter) || letter == '+' || letter == '-' || letter == '*' || letter == '/' || letter == '.';
}

int CountParams(memptr sexpr, int *error)
{
    memptr tmp;
    int counter = 0;
    tmp = MemoryPool[sexpr].cdr;
    while (tmp != 0) {
        if (MemoryPool[tmp].carKind != CONS && MemoryPool[tmp].cdrKind != CONS) {
            *error = 1;
            printf("Malformed parameter list : ");
            Print(sexpr);
            printf("\n");
        } else {
            tmp = MemoryPool[tmp].cdr;
            counter++;
        }
    }
    return counter;
}

void PrintCall(memptr sexpr)
{
    int i;
    for (i = 0; i < indent; i++)
        printf(" ");
    printf("Call: ");
    PrintTr(sexpr);
    printf("\n");
}

void PrintExit(memptr sexpr)
{
    int i;
    for (i = 0; i < indent - 1; i++)
        printf(" ");
    printf("Return from %s = ", AtomHandles[MemoryPool[sexpr].car].name);
    Print(callStack[SP - 1]);
    printf("\n");
}

void PrintListTr(int first, memptr sexpr)
{
    if (sexpr != 0) {
        if (MemoryPool[sexpr].carKind != CONS && MemoryPool[sexpr].cdrKind != CONS) {
            printf(" . ");
            PrintTr(sexpr);
        } else {
            if (!first)
                printf(" ");
            PrintTr(MemoryPool[sexpr].car);
            PrintListTr(0, MemoryPool[sexpr].cdr);
        }
    }
}

void PrintTr(memptr sexpr)
{
    if (sexpr != 0) {
        if (MemoryPool[sexpr].carKind == STRING) {
            if (AtomHandles[sexpr].value >= 0)
                PrintTr(callStack[FP - AtomHandles[sexpr].value]);
            else
                printf("%s", AtomHandles[sexpr].name);
        } else if (MemoryPool[sexpr].carKind == INTEGER) {
            printf("%d", MemoryPool[sexpr].car);
        } else if (MemoryPool[sexpr].carKind == CONS && MemoryPool[sexpr].cdrKind == CONS) {
            printf("(");
            PrintListTr(1, sexpr);
            printf(")");
        }
        fflush(stdin);
    } else
        printf("()");
}

void Error(char *message, memptr sexpr, int *error)
{
    printf("Error: %s", message);
    PrintTr(sexpr);
    *error = 1;
    callStack[SP++] = 0;
    return;
}
