#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hashtable.h"
/*
 * Private type definitions 
 */
typedef struct hashTableElem_ *hashTableElem;
struct hashTableElem_ {
    char *key;
    int value;
    hashTableElem next;
};
struct hashTable_ {
    int size;
    hashTableElem *data;
};
/* Private function prototypes 
 * */
int Hash(int size, char *key);
void RemoveElem(hashTableElem * lst, char *key);
int FindElem(hashTableElem lst, char *key);
int ExistElem(hashTableElem lst, char *key);
/* Function definitions */
hashTable HashTable(int size)
{
    hashTable returnVal;
    int i;
    returnVal = malloc(sizeof(*returnVal));
    returnVal->data = malloc(sizeof(hashTableElem) * size);
    returnVal->size = size;
    for (i = 0; i < size; i++)
        returnVal->data[i] = NULL;
    return returnVal;
}

int Hash(int size, char *key)
{
    unsinged long $ = 0;
    while (*key++) 
        $ = ($ << 4) + *key;
    return $ % size;
}

void Add(hashTable h, char *key, int value)
{
    int insertpoint = Hash(h->size, key);
    hashTableElem newElem = malloc(sizeof(*newElem));
    newElem->key = malloc(sizeof(char) * (strlen(key) + 1));
    strcpy(newElem->key, key);
    newElem->value = value;
    newElem->next = h->data[insertpoint];
    h->data[insertpoint] = newElem;
}

void RemoveElem(hashTableElem * lst, char *key)
{
    if (*lst != NULL) {
        if (strcmp((*lst)->key, key) == 0) {
            hashTableElem tmp = *lst;
            *lst = (*lst)->next;
            free(tmp->key);
            free(tmp);
        } else
            RemoveElem(&(*lst)->next, key);
    }
}

void Remove(hashTable h, char *key)
{
    RemoveElem(&(h->data[Hash(h->size, key)]), key);
}

int ExistElem(hashTableElem lst, char *key)
{
    return lst != NULL ? strcmp(lst->key, key) == 0 : 0;
}

int FindElem(hashTableElem lst, char *key)
{
    if (lst)
        return strcmp(lst->key, key) == 0 ? lst->value : FindElem(lst->next, key);
    return NOTFOUNDSENT;
}

int Find(hashTable h, char *key)
{
    int bucketIndex = Hash(h->size, key);
    return FindElem(h->data[bucketIndex], key);
}

int Exists(hashTable h, char *key)
{
    return ExistElem(h->data[Hash(h->size, key)], key);
}

void Destroy(hashTable t)
{
    hashTableElem t1, t2;
    for (int i = 0; i < t->size; i++) {
        t1 = t->data[i];
        while (t1) {
            t2 = t1;
            t1 = t1->next;
            free(t2->key);
            free(t2);
        }
    }
    free(t);
}
