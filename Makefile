lisp: mainloop.o memmanage.o hashtable.o
	cc -std=c99 -g -w -o Mylisp mainloop.o memmanage.o hashtable.o

mainloop.o: mainloop.c memmanage.h hashtable.h
	cc -g -c -w -std=c99 mainloop.c

memmanage.o: memmanage.h memmanage.c
	cc -g -c -w -std=c99 memmanage.c

hashtable.o: hashtable.h hashtable.c
	cc -g -c -w -std=c99 hashtable.c

clean:
	rm -f Mylisp *.o core *~

indent: 
	indent -nut -ts2 -kr -l1000 *.[ch]

wc: 
	wc *.[ch]
