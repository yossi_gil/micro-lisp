(defun comp (a b)
  (cond ((= a b) 0)
	((> a b) 1)
	(t -1)))

(defun factorial (n)
  (if (= n 0) 1 (* n (factorial (- n 1)))))
	
(defun add1 (n) (+ n 1))

(defun pow (n e)
  (if (= e 1) n (* n (pow n (- e 1)))))	
  
(defun fibo (n)
  (cond ((= n 0) 0)
    ((= n 1) 1)
	(t (+ (fibo (- n 1)) (fibo (- n 2))))))

(defun sumSquare (index n)
   (if (> index n) 0
	(+ (pow index 2) (sumSquare(+ 1 index) n))))
