(defun length (a)
  (cond ((null a) 0)
	(t (+ 1 (length (cdr a))))))

(defun append (a b)
  (cond ((null a) b)
	(t (cons (car a) (append (cdr a) b)))))

(defun cons-to-end (a b)
  (cond ((null b) (cons a b))
        ((atom b) (cons b a))
        (t (cons (car b) (cons-to-end a (cdr b))))))

(defun second (a)
   (car (cdr a)))

(defun cadr (a)
  (car (cdr a)))

(defun caddr (a)
  (car (cdr (cdr a))))

(defun cadddr (a)
  (car (cdr (cdr (cdr a)))))

(defun caar (a)
  (car (car a)))

(defun caadr (a)
  (car (car (cdr a))))

(defun caaddr (a)
  (car (car (cdr (cdr a)))))

(defun caadddr (a)
  (car (car (cdr (cdr (cdr a))))))

