#include "memmanage.h"
#include <stdlib.h>
memptr freelist;
/* Prototypes (for functions privite to the memory manager */
/*             that to not appear in the memanage.h file  */
/* Function     : DestroyCell                             */
/* Input Params : mem - the memory cell to free           */
/* Return Value : none                                    */
/* Purpose      : Thus function frees any C memory  (not  */
/*                lisp memory) that was allocated for     */
/*                storing atom names                      */
void DestroyCell(memptr mem);
/* ----------------------------------------------------- */
/* Function Definitions                                  */
/* Function     : DestroyCell                             */
/* Input Params : mem - the memory cell to free           */
/* Return Value : none                                    */
/* Purpose      : Thus function frees any C memory  (not  */
/*                lisp memory) that was allocated for     */
/*                storing atom names                      */
void DestroyCell(memptr mem)
{
    if (MemoryPool[mem].carKind == STRING) {
        free((void *) AtomHandles[mem].name);
        AtomHandles[mem].name = NULL;
        AtomHandles[mem].value = 0;
    } else {
        MemoryPool[mem].carKind = STRING;
        MemoryPool[mem].cdrKind = NIL;
        MemoryPool[mem].cdr = 0;
        MemoryPool[mem].car = 0;
        AtomHandles[mem].name = NULL;
        AtomHandles[mem].value = 0;
    }
}

/* Function:      PrintMemory                            */
/* Input Params:  memsize -- the number of memory blocks */
/*                           to print                    */
/* Return Value:  none                                   */
/* Purpose:       This function prints the first memize  */
/*                memory locations to the screen         */
void PrintMemory(int memsize)
{
    int i;
    if (memsize >= MEMSIZE)
        memsize = memsize - 1;
    printf("   Call Stack Function Stack   Memory    (FreelistHead = %.3d) \n", GcObject.headFreeList->memEntryNum);
    printf("   ---------- --------------  ----------------------------\n");
    for (i = 0; i < memsize; i++) {
        if ((i == SP) && (i == FP))
            printf("SP->FP->");
        else if (i == SP)
            printf("    SP->");
        else if (i == FP)
            printf("    FP->");
        else
            printf("        ");
        if (i <= SP)
            printf("%.3d", callStack[i]);
        else
            printf("   ");
        if (i <= functionStackPtr)
            printf("      %.3d  ", functionStack[i]);
        else
            printf("           ");
        printf("        %.3d : ", i + 1);
        if (MemoryPool[i + 1].carKind == STRING) {
            printf("A | %s | %d \n", AtomHandles[i + 1].name, AtomHandles[i + 1].value);
        } else if (MemoryPool[i + 1].carKind == INTEGER) {
            printf("N | %d \n", MemoryPool[i + 1].car);
        } else if (MemoryPool[i + 1].carKind == CONS && MemoryPool[i + 1].cdrKind == CONS) {
            printf("L | %.3d | %.3d \n", MemoryPool[i + 1].car, MemoryPool[i + 1].cdr);
        }
    }
}

void InitGcObject(void)
{
    /* GC Init: */
    GcObject.headUsedList = (gc_list_t *) malloc(sizeof(gc_list_t));
    GcObject.headUsedList->memEntryNum = 0;
    GcObject.headUsedList->next = NULL;
    GcObject.headUsedList->prev = NULL;
    GcObject.headUsedList = GcObject.headUsedList;
    GcObject.usedListCnt = 1;
    GcObject.gcArray[0].gcBit = 1;
    GcObject.gcArray[0].ptrToGcNode = GcObject.headUsedList;
    GcObject.gcArray[0].isInUsedList = true;
    GcObject.headFreeList = NULL;
    GcObject.freeListCnt = 0;
    for (int i = MEMSIZE - 1; i > 0; i--) {
        gc_list_t *temp = (gc_list_t *) malloc(sizeof(gc_list_t));
        temp->memEntryNum = i;
        temp->next = GcObject.headFreeList;
        temp->prev = NULL;
        if (GcObject.headFreeList != NULL) {
            GcObject.headFreeList->prev = temp;
        }
        GcObject.headFreeList = temp;
        GcObject.freeListCnt++;
        GcObject.gcArray[i].gcBit = 0;
        GcObject.gcArray[i].ptrToGcNode = temp;
        GcObject.gcArray[i].isInUsedList = false;
    }
    GcObject.headFreeList = GcObject.headFreeList;
}

/*  Function:     InitMemorySystem                      */
/*  Input Params: None                                  */
/*  Return Value: None                                  */
/*  Purpose:      This function must be called before   */
/*                any other memory functions (New,      */
/*                PrintMemory, CollectGarbage) are      */
/*                called.  This function intializes the */
/*                freelist, and does any other necessary*/
/*                initialization                        */
void InitMemorySystem(void)
{
    SP = 0;
    FP = 0;
    functionStackPtr = 0;
    MemoryPool[0].carKind = NIL;
    MemoryPool[0].car = 0;
    MemoryPool[0].cdrKind = NIL;
    MemoryPool[0].cdr = 0;
    SP++;
    for (int i = 1; i < MEMSIZE; i++) {
        MemoryPool[i].carKind = STRING;
        MemoryPool[i].cdrKind = NIL;
        MemoryPool[i].car = i;
        MemoryPool[i].cdr = 0;
        AtomHandles[i].name = NULL;
        AtomHandles[i].value = 0;
    }
    //freelist = 1;// Memory[0];
    InitGcObject();
}

/* Function:      New                                    */
/* Input Params:  None                                   */
/* Return Value:  a pointer to a free block of memory    */
/* Purpose:       This function returns a pointer to a   */
/*                free memory block (possibly doing some */
/*                garbage collection to free up some     */
/*                memory first                           */
memptr New(void)
{
    if (GcObject.headFreeList == NULL)
      CollectGarbage();
    if (GcObject.headFreeList == NULL) {
      fprintf(stderr, "Memory exhausted\n");
      exit(0);
    }
    gc_list_t *temp = GcObject.headFreeList->next;
    GcObject.headFreeList->next = GcObject.headUsedList;
    GcObject.headUsedList->prev = GcObject.headFreeList;
    GcObject.usedListCnt++;
    GcObject.headFreeList->prev = NULL;
    GcObject.headUsedList = GcObject.headFreeList;
    GcObject.headFreeList = temp;
    GcObject.freeListCnt--;
    GcObject.gcArray[GcObject.headUsedList->memEntryNum].gcBit = 0;
    GcObject.gcArray[GcObject.headUsedList->memEntryNum].isInUsedList = true;
    return GcObject.headUsedList->memEntryNum;
}

/* Function:      CollectGarbage                         */
/* Input Params:  none                                   */
/* Return Value:  none                                   */
/* Purpose:       This function forces a garbage         */
/*                collection                             */
void CollectGarbage(void)
{
    int cntOld = GcObject.freeListCnt;
    /* Find roots: */
    gc_list_t *roots = findRoots();
    /* Iterate roots and mark recursively: */
    gc_list_t *curr = roots;
    while (curr != NULL) {
        mark(curr->memEntryNum);
        curr = curr->next;
    }
    free(roots);
    /* Sweep - Finally arrange the gc object(freeList and usedList): */
    sweep();
    printf("*** The Garbage Collector free %d memory entries *** \n", GcObject.freeListCnt - cntOld);
}

gc_list_t *findRoots(void)
{
    gc_list_t *rootsList = (gc_list_t *) malloc(sizeof(gc_list_t));
    rootsList->memEntryNum = 0;
    rootsList->next = rootsList->prev = NULL;
    for (int i = SP; i > 0; i--) {
        gc_list_t *temp = (gc_list_t *) malloc(sizeof(gc_list_t));
        temp->memEntryNum = callStack[i];
        temp->next = rootsList;
        temp->prev = NULL;
        rootsList->prev = temp;
        rootsList = temp;
    }
    for (int i = functionStackPtr - 1; i >= 0; i--) {
        gc_list_t *temp = (gc_list_t *) malloc(sizeof(gc_list_t));
        temp->memEntryNum = functionStack[i];
        temp->next = rootsList;
        temp->prev = NULL;
        rootsList->prev = temp;
        rootsList = temp;
    }
    return rootsList;
}

void mark(memptr memEntryNum)
{
    if (GcObject.gcArray[memEntryNum].gcBit == 1)
        return;
    GcObject.gcArray[memEntryNum].gcBit = 1;
#if PRINT_DEBUG_GC
    printf("[visiting] %d: \t", memEntryNum);
    if (MemoryPool[memEntryNum].carKind == STRING) {
        printf("A | %s | %d \n", AtomHandles[memEntryNum].name, AtomHandles[memEntryNum].value);
    } else if (MemoryPool[memEntryNum].carKind == INTEGER) {
        printf("N | %d \n", MemoryPool[memEntryNum].car);
    } else if (MemoryPool[memEntryNum].carKind == CONS && MemoryPool[memEntryNum].cdrKind == CONS) {
        printf("L | %.3d | %.3d \n", MemoryPool[memEntryNum].car, MemoryPool[memEntryNum].cdr);
    }
    //print(memEntryNum);
    printf("\n");
#endif
    if (MemoryPool[memEntryNum].carKind == CONS && MemoryPool[memEntryNum].cdrKind == CONS) {
        if (MemoryPool[memEntryNum].car != 0) {
            mark(MemoryPool[memEntryNum].car);
        }
        if (MemoryPool[memEntryNum].cdr != 0) {
            mark(MemoryPool[memEntryNum].cdr);
        }
    }
}

void sweep(void)
{
    for (int i = MEMSIZE - 1; i >= 1; i--) {
        if (GcObject.gcArray[i].gcBit == 0 && GcObject.gcArray[i].isInUsedList == true) {
            gc_list_t *temp = GcObject.gcArray[i].ptrToGcNode;
            if (temp->prev != NULL)
                temp->prev->next = temp->next;
            if (temp->next != NULL)
                temp->next->prev = temp->prev;
            DestroyCell(i);
#if PRINT_DEBUG_GC
            printf("[freeing] %d\n", i);
#endif
            GcObject.usedListCnt--;
            //<TODO> free symbols
            temp->prev = NULL;
            temp->next = GcObject.headFreeList;
            if (GcObject.headFreeList != NULL) {
                GcObject.headFreeList->prev = temp;
            }
            GcObject.headFreeList = temp;
            GcObject.freeListCnt++;
            GcObject.gcArray[i].isInUsedList = false;
        } else
            GcObject.gcArray[i].gcBit = 0;
    }
}

static int firstFreeCons;
static int nextFreeCons(int currentCons) {
  return (int) MemoryPool[currentCons];
}


int newCons() {
  int $ = firstFreeCons;
  firstFreeCons = nextFreeCons(firstFreeCons); 
  return $;
}

void deleteCons(int consIndex) {
  (int) MemoryPool[consIndex] = firstFreeCons;
  firstFreeCons = consIndex; 
}



