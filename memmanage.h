#include <stdbool.h>
#define PRINT_DEBUG_GC   0
enum {
    // How many bits for index into pool:
    LG2_POOLSIZE = 14,
    // How many bits for storing car/cdr kind:
    KIND_SIZE = 2
};
enum kind { NIL = 0, STRING, INTEGER, CONS };
typedef struct Cons_t {
    unsigned int carKind:KIND_SIZE;
    unsigned int car:LG2_POOLSIZE;
    unsigned int cdrKind:KIND_SIZE;
    unsigned int cdr:LG2_POOLSIZE;
} Cons;









typedef struct AtomElem_t {
    const char *name;
    int value;
} AtomElem;

/* Macro definitions: */
#define MEMSIZE (1 << LG2_POOLSIZE) 
#define MAXSTRINGSIZE 30
#define DATASTACKSIZE 500
#define FUNCTIONSTACKSIZE 100
typedef unsigned int memptr;
//typedef int memptr;
/*** Type Definitions: ***/
  /* Garbage Collector */
typedef struct gc_list_s {
    memptr memEntryNum;
    struct gc_list_s *next;
    struct gc_list_s *prev;
} gc_list_t;
typedef struct gc_array_s {
    gc_list_t *ptrToGcNode;
    bool isInUsedList;
    unsigned int gcBit:1;
} gc_array_t;
typedef struct gcObject_s {
    gc_list_t *headFreeList;
    int freeListCnt;
    gc_list_t *headUsedList;
    int usedListCnt;
    gc_array_t gcArray[MEMSIZE];
} gcObject_t;
/* A few addional type definitions: */
#if 0
struct memcell {
    int used;
    enum { Atom, List, Number } kind;
    union {
        struct {
            memptr car;
            memptr cdr;
        } list;
        struct {
            char *name;
            int value;
        } atom;
        int number;
    } u;
};
#endif
/*** Globals ***/
AtomElem AtomHandles[MEMSIZE];  /* Will be used for atoms: */
Cons MemoryPool[MEMSIZE];       /* Memory Pool of struct Cons nodes - Virtualization of Heap */


int newCons(); 
void deleteCons(int consIndext);



//struct memcell Memory[MEMSIZE];                 /* Heap  */
memptr callStack[DATASTACKSIZE];  /* Call stack */
memptr functionStack[FUNCTIONSTACKSIZE];  /* Function Stack */
int SP;                         /* Stack Pointer */
int FP;                         /* Frame Pointer */
int functionStackPtr;           /* Function stack pointer */
gcObject_t GcObject;            /* Garbage Collector Object */
/*** Garbage Collector Function Prototypes: ***/
void InitGcObject(void);
gc_list_t *findRoots(void);
void mark(memptr memEntryNum);
void sweep(void);
/*** Other Function Prototypes ***/
/*  Function:     InitMemorySystem                      */
/*  Input Params: None                                  */
/*  Return Value: None                                  */
/*  Purpose:      This function must be called before   */
/*                any other memory functions (New,      */
/*                PrintMemory, CollectGarbage) are      */
/*                called.  This function intializes the */
/*                freelist, and does any other necessary*/
/*                initialization                        */
void InitMemorySystem(void);
/* Function:      New                                    */
/* Input Params:  None                                   */
/* Return Value:  a pointer to a free block of memory    */
/* Purpose:       This function returns a pointer to a   */
/*                free memory block (possibly doing some */
/*                garbage collection to free up some     */
/*                memory first                           */
memptr New(void);
/* Function:      PrintMemory                            */
/* Input Params:  memsize -- the number of memory blocks */
/*                           to print                    */
/* Return Value:  none                                   */
/* Purpose:       This function prints the first memize  */
/*                memory locations to the screen         */
void PrintMemory(int memsize);
/* Function:      CollectGarbage                         */
/* Input Params:  none                                   */
/* Return Value:  none                                   */
/* Purpose:       This function forces a garbage         */
/*                collection                             */
void CollectGarbage(void);
