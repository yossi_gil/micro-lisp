#define NOTFOUNDSENT -999
typedef struct hashTable_ *hashTable;
/*  Function:     HashTable (constructor)                */
/*  Input Params: size, the size for the new table       */
/*  Return Value: a pointer to the created table         */
/*  Purpose:      This function creates a new hash table */
hashTable HashTable(int size);
/*  Function:     Add                                    */
/*  Input Params: h, the table to add the element to     */
/*                key, the key of the element            */
/*                value, the element to add              */
/*  Return Value: none                                   */
/*  Purpose:      This function adds the element value   */
/*                (with key value key) to the table h    */
void Add(hashTable h, char *key, int value);
/*  Function:     Remove                                   */
/*  Input Params: h, the table to remove the element from  */
/*                key, the key of the element to remove    */
/*  Return Value: none                                     */
/*  Purpose:      This function removes the element        */
/*                with key value key from the table h      */
void Remove(hashTable h, char *key);
/*  Function:     Find                                     */
/*  Input Params: h, the table to find the element in      */
/*                key, the key of the element to find      */
/*  Return Value: the value of the element with key        */
/*                value key                                */
/*  Purpose:      This function returns the element with   */
/*                key value key, or NOTFOUNDSENT if the    */
/*                element does not exist                   */
int Find(hashTable h, char *key);
/*  Function:     Exists                                     */
/*  Input Params: h, the table to find the element in        */
/*                key, the key of the element to find        */
/*  Return Value: 1 if the element w/ key value key is in    */
/*                 the table, 0 otherwise                    */
/*  Purpose:      This function returns true iff the element */
/*                with key value key is in the table         */
int Exists(hashTable h, char *key);
/*  Function:     Destroy                                     */
/*  Input Params: h, the table to destroy                     */
/*  Purpose:      Frees all memory created dynamically for h  */
void Destroy(hashTable t);
